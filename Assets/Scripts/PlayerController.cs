using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    
    private Vector2 _movementDirection = new Vector2();
    private Vector2 _lookDirection = new Vector2();

    public Camera playerMainCamera;

    public float movementSpeed = 10f;
    public float mouseSensitivity = 20f;

    public void OnMove(InputAction.CallbackContext context)
    {
        _movementDirection = context.ReadValue<Vector2>().normalized;
    }
    public void OnLook(InputAction.CallbackContext context)
    {
        _lookDirection = context.ReadValue<Vector2>();
    }
    public void OnInteract(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            RaycastHit hit;

            Ray ray = playerMainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            if (Physics.Raycast(ray, out hit, 1.5f))
            {
                IInteractable interactableObject = hit.collider.transform.GetComponent<IInteractable>();

                if (interactableObject != null)
                {
                    interactableObject.Interact();
                }
            }
        }
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        UpdateMovement();
    }

    void UpdateMovement()
    {
        Vector3 movementVector = (transform.forward * _movementDirection.y + transform.right * _movementDirection.x) * movementSpeed;

        GetComponent<Rigidbody>().velocity = movementVector;

        float verticalAngle = _lookDirection.x * mouseSensitivity * Time.deltaTime;
        float horizontalAngle = _lookDirection.y * mouseSensitivity * Time.deltaTime;

        transform.Rotate(Vector3.up, verticalAngle);

        playerMainCamera.transform.localEulerAngles -= Vector3.right * horizontalAngle;
    }
}
