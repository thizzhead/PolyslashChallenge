using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorwayController : MonoBehaviour, IInteractable
{
    public GameObject elevatorObject;
    public int floorIndex;
    public DoorController doorController;
    public AudioLibrary audioLibrary;
    private ElevatorController _elevatorController;
    private AudioSource _audioSource;

    void Start()
    {
        doorController = GetComponent<DoorController>();
        _elevatorController = elevatorObject.GetComponent<ElevatorController>();
        _audioSource = GetComponent<AudioSource>();
    }

    public void Interact()
    {
        if (floorIndex == _elevatorController.currentFloor)
        {
            if (!doorController.isAnimating)
            {
                if (!doorController.isOpen)
                {
                    doorController.OpenDoor();
                    _elevatorController.doorController.OpenDoor();
                }
                else
                {
                    doorController.CloseDoor();
                    _elevatorController.doorController.CloseDoor();
                }
            }
        }
        else
        {
            _elevatorController.MoveElevatorToFloor(floorIndex);
        }
    }

    public void PlayAudioClip(string name)
    {
        AudioClip clip = audioLibrary.GetClipByKey(name);

        if (clip != null)
        {
            _audioSource.clip = clip;
            _audioSource.loop = false;
            _audioSource.Play();
        }
    }
    public void StopAudio()
    {
        _audioSource.Stop();
    }
}
