using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AudioLibrary
{
    public List<string> keys;
    public List<AudioClip> values;

    public AudioClip GetClipByKey(string key)
    {
        return values[key.IndexOf(key)];
    }
}
