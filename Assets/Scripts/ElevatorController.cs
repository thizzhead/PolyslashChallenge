using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorController : MonoBehaviour
{
    public float elevatorSpeed = 10f;
    public int currentFloor;
    public AudioLibrary audioLibrary;
    public List<GameObject> elevatorDoorways = new List<GameObject>();
    [HideInInspector]
    public DoorController doorController;
    private bool _isMoving;
    private AudioSource _audioSource;

    private void Start()
    {
        doorController = GetComponent<DoorController>();
        _audioSource = GetComponent<AudioSource>();
    }

    public void MoveElevatorToFloor(int floorIndex)
    {
        if (currentFloor != floorIndex && !_isMoving)
        {
            if (doorController.isOpen)
            {
                doorController.CloseDoor();
            }
            if (elevatorDoorways[currentFloor].GetComponent<DoorController>().isOpen)
            {
                elevatorDoorways[currentFloor].GetComponent<DoorController>().CloseDoor();
            }

            StartCoroutine(ExecuteElevatorMovement(floorIndex));
        }
    }

    private IEnumerator ExecuteElevatorMovement(int floorIndex)
    {
        Vector3 targetPosition = elevatorDoorways[floorIndex].transform.position;

        AudioClip clip = audioLibrary.GetClipByKey("ElevatorRide");

        if (clip != null)
        {
            _audioSource.clip = clip;
            _audioSource.loop = true;
            _audioSource.Play();
        }

        while (transform.position != targetPosition)
        {
            _isMoving = true;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, elevatorSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        _audioSource.Stop();

        _isMoving = false;
        currentFloor = floorIndex;
        doorController.OpenDoor();
        elevatorDoorways[currentFloor].GetComponent<DoorController>().OpenDoor();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            if (!doorController.isOpen && !_isMoving)
            {
                doorController.OpenDoor();
                elevatorDoorways[currentFloor].GetComponent<DoorController>().OpenDoor();
            }
        }
    }

}
