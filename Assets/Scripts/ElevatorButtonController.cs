using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorButtonController : MonoBehaviour, IInteractable
{
    public Transform elevatorObject;
    public int buttonFloorIndex;

    public void Interact()
    {
        elevatorObject.GetComponent<ElevatorController>().MoveElevatorToFloor(buttonFloorIndex);
    }

}
