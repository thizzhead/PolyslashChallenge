using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public bool isOpen;
    public bool isAnimating;
    private bool _doorTimerStarted;

    public void DoorOpened()
    {
        isOpen = true;
        isAnimating = false;
    }
    public void DoorClosed()
    {
        isOpen = false;
        isAnimating = false;
    }
    public void IsAnimating()
    {
        isAnimating = true;
    }


    public void OpenDoor()
    {
        if (!isAnimating)
        {
            GetComponent<Animator>().Play("OpenDoor", 0, 0.0f);
            StartCoroutine(CloseDoorTimer());
        }
    }
    public void CloseDoor()
    {
        if (!isAnimating)
        {
            GetComponent<Animator>().Play("CloseDoor", 0, 0.0f);
            if (_doorTimerStarted)
            {
                StopCoroutine(CloseDoorTimer());
            }
        }
    }

    public IEnumerator CloseDoorTimer()
    {
        _doorTimerStarted = true;

        yield return new WaitForSeconds(10f);

        if (isOpen)
        {
            CloseDoor();
        }

        _doorTimerStarted = false;
    }
}
